ARG DEBIAN_VERSION="bookworm"

FROM debian:${DEBIAN_VERSION}

ARG WORKDIR="/var/www"

ENV WORKDIR ${WORKDIR}

RUN apt-get update -y && \
    apt-get install -y --no-install-recommends \
                    curl \
                    apt-transport-https \
                    lsb-release \
                    ca-certificates \
                    lsb-release \
                    gnupg && \
    curl https://nginx.org/keys/nginx_signing.key | apt-key add - && \
    echo "deb http://nginx.org/packages/debian $(lsb_release -sc) nginx" >> /etc/apt/sources.list.d/nginx.list && \
    apt-get update -y && \
    apt-get install -y --no-install-recommends nginx && \
    rm -r /var/lib/apt/lists/* && \
    mkdir /etc/nginx/cache/ && \
    mkdir /etc/nginx/cache/fastcgi/ && \
    mkdir /etc/nginx/cache/proxy/ && \
    mkdir /var/www/error-pages && \
    rm /etc/nginx/conf.d/default.conf

COPY config/nginx.conf /etc/nginx/nginx.conf
COPY ./error-pages/ /var/www/error-pages


WORKDIR /var/www

# Add Dockerfile to the container
# It helps for debugging & maintainability + is considered good practice
COPY Dockerfile /

CMD ["/usr/sbin/nginx", "-c", "/etc/nginx/nginx.conf"]
